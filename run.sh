#!/bin/bash

ret=`which docker-compose`

if [ ret -ne 0 ]; 
   echo "Could not find docker-compose command"
   exit -1
fi

cd atlassian
docker-compose up -d
cd ../jenkins
docker-compose up -d
