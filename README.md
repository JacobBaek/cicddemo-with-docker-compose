# This docker-compose script that can run CICD demo with Atlassian Product

# This script included below products
- Jira (with PostgresDB)
- Confluence (not be created Database)
- Bitbucket (not be created Database)
- Jenkins

# How to use
This script doesn't have to get and write the license number.
So you should get the license number from my.atlassian.com.

# Future plan
- Bamboo will be parted in this script.
- Database of Bitbucket and Confluence will be parted in this script.

